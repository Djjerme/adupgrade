﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace ADUpgrade
{
    class Program
    {
        static void Main(string[] args)
        {
            var i = 0;
            SqlConnection conn = new SqlConnection(@"Data Source=ARAS-AG;Initial Catalog=Innovator;user id=innovator;password=Sp4ceAg3Ve11um");
            conn.Open();
            OleDbConnection Xcon = new OleDbConnection(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=S:\IT\JPlance\ADUpgrade.xlsx;Extended Properties=Excel 12.0");
            OleDbDataAdapter Xda = new OleDbDataAdapter("select * from [Sheet1$]", Xcon);
            DataTable Xdt = new DataTable();
            Xda.Fill(Xdt);
            try
            {
                foreach (DataRow row in Xdt.Rows)
                {
                    string sql = string.Format(@"update [Innovator].[USER] SET LOGIN_NAME = '{0}' where LOGIN_NAME = '{1}'; update [Innovator].[IDENTITY] SET NAME = '{0}', KEYED_NAME = '{0}' where NAME = '{1}';", row[1].ToString(), row[0].ToString());
                    SqlCommand cmd = new SqlCommand(sql, conn);
                    int a = cmd.ExecuteNonQuery();
                    if (a != 0)
                    {
                        Console.WriteLine(row[0].ToString() + ", " + row[1].ToString() + ", success");
                        i++;
                    }
                }
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }
            finally { conn.Close(); }
            Console.WriteLine(string.Format("{0} records updated", i));
            Console.Read();
        }
    }
}
